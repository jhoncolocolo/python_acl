from models.role import Role as RoleModel
from schemas.role import Role

class RoleService():
    def __init__(self,db) -> None:
        self.db = db

    def getRoles(self):
        result = self.db.query(RoleModel).all()
        return result
    
    def getRole(self,id:int):
        result = self.db.query(RoleModel).filter(RoleModel.id == id).first()
        return result
    
    def getRolesByName(self,name:str):
        result = self.db.query(RoleModel).filter(RoleModel.name==name).all()
        return result
    
    def createRole(self,role:Role):
        new_role = RoleModel(**role.dict())
        self.db.add(new_role)
        self.db.commit()
        return 

    def updateRole(self,id:int,data:Role):
        model = self.db.query(RoleModel).filter(RoleModel.id == id).first()
        model.name = data.name
        model.role = data.role
        model.description = data.description
        self.db.commit()
        return

    def deleteRole(self,id:int):
        self.db.query(RoleModel).filter(RoleModel.id == id).delete()
        self.db.commit()
        return