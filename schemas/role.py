from typing import  Optional
from pydantic import BaseModel, Field

class Role(BaseModel):
    id: Optional[int] = None
    name: str = Field(min_length=7,max_length=40)
    role: str = Field(min_length=5,max_length=30)
    description: str = Field(min_length=7,max_length=75)

    class Config:
        json_schema_extra = {
            "example": {
                "id":1,
                "name": "Un Nombre",
                "role": "Un Role",
                "description": "Una Descripción"
            }
        }