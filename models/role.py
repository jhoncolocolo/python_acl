from config.database import Base
from sqlalchemy import Column,Integer,String

class Role(Base):

    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    role = Column(String)
    description = Column(String)