from fastapi import APIRouter
from fastapi import Depends,Path, Query
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.role import Role as RoleModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.role import RoleService
from schemas.role import Role


role_router = APIRouter()

@role_router.get('/roles',tags=['roles'],response_model=List[Role],status_code=200,dependencies=[Depends(JWTBearer())])
def get_roles() -> List[Role] :
    db  = Session()
    result = RoleService(db).getRoles()
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

@role_router.get('/roles/{id}',tags=['roles'], response_model=Role)
def get_role(id:int= Path(ge=1,le=2000)) -> Role:
    db  = Session()
    result = RoleService(db).getRole(id)
    if not result:
        return JSONResponse(status_code=404,content={'message':"Not Found"})
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

@role_router.get('/roles/',tags=['roles'],response_model=List[Role])
def get_role_by_names(name: str = Query(min_length=7,max_length=40)) -> List[Role]:
    db  = Session()
    result = RoleService(db).getRolesByName(name)
    return JSONResponse(status_code=200,content=jsonable_encoder(result)) 

@role_router.post('/roles',tags=['roles'], response_model=dict,status_code=201)
def create_role(role:Role) -> dict:
    db  = Session()
    RoleService(db).createRole(role)
    return JSONResponse(status_code=201,content={"message":"Se ha registrado el Rol"}) 


@role_router.put('/roles/{id}',tags=['roles'], response_model=dict,status_code=200)
def update_role(id : int, role: Role) -> dict :
    db  = Session()
    result = RoleService(db).getRole(id)
    if not result:
        return JSONResponse(status_code=404,content={'message':"Not Found"})
    RoleService(db).updateRole(id,role)
    return JSONResponse(status_code=200, content={"message":"Se ha modificado el Rol"}) 

@role_router.delete('/roles/{id}',tags=['roles'], response_model=dict,status_code=200)
def delete_role(id : int) -> dict :
    db  = Session()
    role_model = db.query(RoleModel).filter(RoleModel.id==id).first()
    if not role_model:
        return JSONResponse(status_code=404,content={'message':"Not Found"})
    RoleService(db).deleteRole(id)
    return JSONResponse(status_code=200,content={"message":"Se ha eliminado el Rol"}) 