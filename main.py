from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine,Base
from middlewares.error_handler import ErrorHandler
from routers.role import role_router
from routers.user import user_router

app = FastAPI()
app.title = "Mi primer  aplicaciòn"
app.version = "0.0.0"
app.add_middleware(ErrorHandler)
app.include_router(role_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

@app.get('/',tags=['HOME'])
def message():
    return HTMLResponse('<h1>Como me va mal</h1>')